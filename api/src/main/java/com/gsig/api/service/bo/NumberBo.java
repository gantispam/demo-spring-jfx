package com.gsig.api.service.bo;

import javax.validation.constraints.NotNull;

/**
 * Number model.
 * This model is mapping with NumberModel from REST API.
 *
 * @author GSI
 * @since 17/10/2019
 */
public class NumberBo {

    @NotNull
    private Float numberOne;
    @NotNull
    private Float numberTwo;

    /**
     * Cumpute result from API.
     */
    private Float result;

    public NumberBo() {
    }

    public NumberBo(Float number1, Float number2) {
        this.numberOne = number1;
        this.numberTwo = number2;
    }

    public Float getNumberOne() {
        return numberOne;
    }

    public void setNumberOne(Float numberOne) {
        this.numberOne = numberOne;
    }

    public Float getNumberTwo() {
        return numberTwo;
    }

    public void setNumberTwo(Float numberTwo) {
        this.numberTwo = numberTwo;
    }

    public Float getResult() {
        return result;
    }

    public void setResult(Float result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "NumberModel{" +
                "numberOne=" + numberOne +
                ", numberTwo=" + numberTwo +
                ", result=" + result +
                '}';
    }
}
