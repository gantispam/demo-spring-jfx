package com.gsig.api.service;

import com.gsig.api.service.bo.NumberBo;
import org.springframework.stereotype.Service;

/**
 * Sample service.
 *
 * @author GSI
 * @since 17/10/2019
 */
@Service
public class NumberService {

    /**
     * Do my business logic here.
     *
     * @param numberModel Numbers to compute
     * @return NumberModel with result
     */
    public NumberBo computeNumber(NumberBo numberModel) {
        // business logic here
        numberModel.setResult(numberModel.getNumberOne() * numberModel.getNumberTwo());
        return numberModel;
    }

}
