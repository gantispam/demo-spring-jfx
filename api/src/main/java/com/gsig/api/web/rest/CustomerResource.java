package com.gsig.api.web.rest;

import com.gsig.api.service.NumberService;
import com.gsig.api.service.bo.NumberBo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URISyntaxException;

/**
 * REST controller for managing {@link NumberBo}.
 *
 * @author GSI
 * @since 17/10/2019
 */
@RestController
@RequestMapping("/api")
public class CustomerResource {

    private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

    private final NumberService sampleService;

    public CustomerResource(NumberService sampleService) {
        this.sampleService = sampleService;
    }

    /**
     * {@code POST  /number} : Compute numbers
     *
     * @param numberBo the numberBo to compute.
     * @return the {@link ResponseEntity} with status {@code 200 (Compute)} and with body the new numberBo, or with status {@code 400 (Bad Request)} if the numberBo is wrong.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/numbers")
    public ResponseEntity<NumberBo> createCustomer(@Valid @RequestBody NumberBo numberBo) throws URISyntaxException {
        log.debug("REST request to compute NumberBo : {}", numberBo);
        NumberBo result = sampleService.computeNumber(numberBo);
        return ResponseEntity.ok().body(result);
    }

}
