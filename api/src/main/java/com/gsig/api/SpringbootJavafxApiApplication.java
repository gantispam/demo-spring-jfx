package com.gsig.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entrypoint.
 *
 * @author GSI
 * @since 17/10/2019
 */
@SpringBootApplication
public class SpringbootJavafxApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJavafxApiApplication.class, args);
    }

}
