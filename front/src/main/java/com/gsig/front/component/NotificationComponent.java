package com.gsig.front.component;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * Notification composant.
 * Extract for custom popup later.
 *
 * @author GSI
 * @since 17/10/2019
 */
public class NotificationComponent {

    /**
     * Do not instance this class.
     */
    private NotificationComponent() {
    }

    /**
     * Alert error.
     *
     * @param message Message to display
     */
    public static void error(String message) {
        // trace functionnal errors here with logger
        Alert dialog = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        dialog.show();
    }
}
