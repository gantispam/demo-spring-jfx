package com.gsig.front.service;

import com.gsig.front.domain.NumberModel;
import com.gsig.front.exception.ApiException;
import com.gsig.front.exception.FieldMandatoryException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Sample service.
 *
 * @author GSI
 * @since 17/10/2019
 */
@Service
public class NumberService extends RestService<NumberModel> {

    /**
     * API URL (REST).
     */
    private static final String URI = "numbers";

    /**
     * API URI (REST).
     */
    private String apiUrl;

    public NumberService(@Value("${api.url}") String apiBase) {
        super(NumberModel.class);
        this.apiUrl = apiBase.concat("/").concat(URI);
    }

    /**
     * Business logic.
     *
     * @param numberOne number one
     * @param numberTwo number two
     * @return NumberModel compute by api
     * @throws ApiException            exception from api
     * @throws FieldMandatoryException exception workflow
     */
    public NumberModel computeNumber(String numberOne, String numberTwo) throws ApiException, FieldMandatoryException {

        if (null == numberOne || null == numberTwo || numberOne.isEmpty() || numberTwo.isEmpty()) {
            throw new FieldMandatoryException("Number 1", "Number 2");
        }

        try {
            NumberModel model = new NumberModel(Float.parseFloat(numberOne), Float.parseFloat(numberTwo));
            return apiPost(apiUrl, model);
        } catch (Exception e) {
            // TODO : use logger here
            e.printStackTrace();
            throw new ApiException(e.getMessage());
        }
    }

}
