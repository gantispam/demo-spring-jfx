package com.gsig.front.service;

import com.gsig.front.domain.RestModel;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Abstract service for contact API.
 *
 * @author GSI
 * @since 17/10/2019
 *
 * @param <M>
 */
abstract class RestService<M extends RestModel> {

    private RestTemplate restTemplate;

    private Class<M> classType;

    /**
     * @param classType see RESTMODEL from API.
     */
    RestService(Class<M> classType) {
        this.restTemplate = new RestTemplate();
        this.classType = classType;
    }


    /**
     * Post model on  API.
     *
     * @param uri   to post
     * @param model to post
     * @return response
     */
    M apiPost(String uri, M model) {
        // TODO : use logger here
        System.out.println(String.format("POST on %s model=%s", uri, model));
        return restTemplate.postForObject(uri, new HttpEntity<M>(model), classType);
    }

    // TODO : add other method GET,PUT,DELETE, OPTION, ...
}
