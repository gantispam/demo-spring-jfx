package com.gsig.front.controller;

import com.gsig.front.component.NotificationComponent;
import com.gsig.front.domain.NumberModel;
import com.gsig.front.service.NumberService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.text.Text;
import javafx.util.converter.FloatStringConverter;
import org.springframework.stereotype.Controller;

/**
 * Sample controller for demonstation.
 *
 * @author GSI
 * @since 17/10/2019
 */
@Controller
public class NumberController {

    private static final String ERROR_IS_NULL = "You need to give 2 numbers !";
    private static final String RESULT_MSG = "Result is %s from API!";

    private NumberService sampleService;

    // FORM COMPONENTS

    @FXML
    private TextField numberOne = new TextField();
    @FXML
    private TextField numberTwo = new TextField();

    @FXML
    private Text result;

    public NumberController(NumberService sampleService) {
        this.sampleService = sampleService;
    }

    /**
     * Event : init.
     */
    @FXML
    private void initialize() {
        buildValidatorField();
    }

    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) {
        try {
            NumberModel model = sampleService.computeNumber(numberOne.getText(), numberTwo.getText());
            result.setText(String.format(RESULT_MSG, model.getResult()));
        } catch (Exception e) {
            NotificationComponent.error(e.getMessage());
        }
    }

    /**
     * Build form validator.
     */
    private void buildValidatorField() {
        numberOne.setTextFormatter(new TextFormatter<>(new FloatStringConverter()));
        numberTwo.setTextFormatter(new TextFormatter<>(new FloatStringConverter()));
    }

}
