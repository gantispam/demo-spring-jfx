package com.gsig.front.exception;

/**
 * Custom exception for api comm
 *
 * @author GSI
 * @since 17/10/2019
 */
public class ApiException extends Exception {

    public ApiException(String message) {
        super(String.format("Fail %s", message));
    }
}
