package com.gsig.front.exception;

/**
 * Custom exception for field's format.
 *
 * @author GSI
 * @since 17/10/2019
 */
public class FieldFormatException extends Exception {

    public FieldFormatException(String... fieldName) {
        super(String.format("Field %s is an wrong format", fieldName));
    }
}
