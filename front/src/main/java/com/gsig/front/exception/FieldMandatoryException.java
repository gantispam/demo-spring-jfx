package com.gsig.front.exception;

/**
 * Custom exception for mandatory field.
 *
 * @author GSI
 * @since 17/10/2019
 */
public class FieldMandatoryException extends Exception {

    public FieldMandatoryException(String... fieldName) {
        super(String.format("Field %s is mandatory", fieldName));
    }
}
