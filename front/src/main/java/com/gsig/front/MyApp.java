package com.gsig.front;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Implement SpringBoot on JAVAFX application.
 * This application is only for my demonstration code.
 *
 * @author GSI
 * @since 17/10/2019
 */
@SpringBootApplication
public class MyApp extends Application {

    /**
     * APP title
     */
    private static final String APP_TITLE = "Demonstration by GSIG";
    /**
     * view used as Entrypoint
     */
    private static final String MAIN_VIEW = "/fxml/sample.fxml";

    private ConfigurableApplicationContext springContext;

    private Parent rootNode;

    private FXMLLoader fxmlLoader;

    /**
     * Main entrypoint.
     *
     * @param args cmd arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    @Override
    public void init() throws Exception {
        springContext = SpringApplication.run(MyApp.class);
        fxmlLoader = new FXMLLoader();
        fxmlLoader.setControllerFactory(springContext::getBean);
    }

    /**
     * {@inheritDoc}
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        fxmlLoader.setLocation(getClass().getResource(MAIN_VIEW));
        rootNode = fxmlLoader.load();
        // build scene
        primaryStage.setTitle(APP_TITLE);
        Scene scene = new Scene(rootNode, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() {
        springContext.stop();
    }
}
