# Getting Started

### Description
This is a simple exemple with Springboot integration on JavaFx and Spring REST integration on JavaFx.
* API : springboot + spring web
* FRONT : springboot + JavaFX
* Builder : Gradle

### Running on dev. mode
* API :
```console
cd api && gradle clean && gradle bootRun
```
* FRONT :
```console
cd front && gradle clean && gradle bootRun
```
